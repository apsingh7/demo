In this project a model is used named : User which contains field as: 
id of Type(UUID)
name (String)
age (int)

Another Model Friend is used which contains two fields as 
userId1 (of type UUID) and 
userId2 (of type UUID)

API Details:
1.To Fetch All user details :
HTTP Method : GET
http://localhost:8090/api/user

2.
To Add a user

HTTP Method : POST
End Point : http://localhost:8090/api/user
Payload : User Model As:
{
	"id":"f380b3b1-76d7-4a19-8e64-538b63f3ca15",
         "name": "Ajdddgdgday",
        "age": 2492
}

3. Get USerById :
HttpMethod : GET
EndPoint : http://localhost:8090/api/user/{USerId}

4. Add friend :
HttpMethod : POST
EndPOint : http://localhost:8090/api/user/Friend
Payload : Friend Model As:
{
         "userId1": "67eeb4a3-05ba-4b91-92c4-5806a982d47e",
         "userId2": "eb1e105b-4125-4439-909a-ca3928f919fc"
}
5. See FriendList :
HttpMethod : GET
EndPOint : http://localhost:8090/api/user/{userId}/Friend
6. Connections at K distance :
HttpMethod : GET
EndPoint: http://localhost:8090/api/user/{userId}/Friend/{kDistance}

