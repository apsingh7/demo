package com.example.demo.utilities;
import java.util.*;

public class FriendList<T> {

    private final Map<T, List<T>> map = new HashMap<>();

    public void addVertex(T s) {
        map.put(s, new LinkedList<T>());
    }

    public void addEdge(T source,
                        T destination) {

        if (!map.containsKey(source))
            addVertex(source);

        if (!map.containsKey(destination))
            addVertex(destination);

        map.get(source).add(destination);
        map.get(destination).add(source);
    }


    public void removeFriend(T source,
                             T destination) {

        if (!map.containsKey(source))
            return;

        if (!map.containsKey(destination))
            return;

        map.get(source).remove(destination);
        map.get(destination).remove(source);

    }

    public List<UUID> getFriendListById(T source) {
        List<T> s;
        s = map.get(source);
        List<UUID> friendList = new ArrayList<UUID>();
        if (map.containsKey(source)) {
            s = map.get(source);
            for (int i = 0; i < s.size(); i++) {
                friendList.add((UUID) s.get(i));
            }
        }
        return friendList;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        for (T v : map.keySet()) {
            builder.append(v.toString() + ": ");
            for (T w : map.get(v)) {
                builder.append(w.toString() + " ");
            }
            builder.append("\n");
        }

        return (builder.toString());
    }
}