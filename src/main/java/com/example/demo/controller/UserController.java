package com.example.demo.controller;

import com.example.demo.model.Friend;
import com.example.demo.model.User;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("api/user")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(
        method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public List<User> getAllUsers() {
        return userService.getAllUser();
    }
    @RequestMapping(
        method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE,
            path = "{userId}"
    )
    public User getUserById(@PathVariable("userId") UUID userId) {
        int ss = 0;
        return userService.selectUserById(userId);
    }
    @RequestMapping(
        method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public void insertUser(@RequestBody User user) {

        userService.insertUser(user.getId(),user);
    }

   @RequestMapping(
        method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
           path = "Friend"
    )
    public int addFriend(@RequestBody Friend friend) {
        return userService.addFriend(friend);
    }

    @RequestMapping(
        method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE,
           path = "Friend"
    )
    public int removeFriend(@RequestBody Friend friend) {
        return userService.removeFriend(friend);
    }

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE,
            path = "{userId}/Friend"
    )
    public List<User> getFriendListById(@PathVariable("userId") UUID userId) {
        int ss = 0;
        return userService.friendListById(userId);
    }
}
