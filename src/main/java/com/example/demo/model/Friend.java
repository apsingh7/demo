package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class Friend {
    public UUID userId1;
    public UUID userId2;
    public Friend(
            @JsonProperty("userId1") UUID userId1,
            @JsonProperty("userId2") UUID userId2) {
        this.userId1 = userId1;
        this.userId2 = userId2;
    }
}
