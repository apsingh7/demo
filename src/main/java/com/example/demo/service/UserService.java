package com.example.demo.service;

import com.example.demo.dao.UserDao;
import com.example.demo.model.Friend;
import com.example.demo.model.User;
import com.example.demo.utilities.FriendList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class UserService {

    private final UserDao userDao;

    @Autowired
    public UserService(@Qualifier("userDao") UserDao userDao) {
        this.userDao = userDao;
    }

    public int insertUser(UUID userId, User user) {
        UUID userUid = userId == null ? UUID.randomUUID() : userId;
        user.setId(userUid);
        userDao.insertUser(userUid, user);
        return 1;
    }

    public User selectUserById(UUID userId) {
        return userDao.selectUserById(userId);
    }

    public List<User> getAllUser() {
        return userDao.selectAllUser();
    }

    public int updateUserById(UUID userId, User userUpdate) {
        return userDao.updateUserById(userId, userUpdate);
    }

    public int deleteUserById(UUID userId) {
        return userDao.deleteUserById(userId);
    }

    public int addFriend(Friend friend) {
        return userDao.addFriend(friend);
    }

    public int removeFriend(Friend friend) {
        return userDao.removeFriend(friend);
    }

    public List<User> friendListById(UUID userId) {
        List<UUID> friends = userDao.friendListById(userId);
        List<User> friendListDetails = new ArrayList<User>();
        for (int i = 0; i < friends.size(); i++) {
            friendListDetails.add(userDao.selectUserById((UUID) friends.get(i)));
        }
        return friendListDetails;
    }
}
