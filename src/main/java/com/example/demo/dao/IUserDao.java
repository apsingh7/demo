package com.example.demo.dao;

import com.example.demo.model.Friend;
import com.example.demo.model.User;

import java.util.List;
import java.util.UUID;

public interface IUserDao {
    int insertUser(UUID userId, User user);
    User selectUserById(UUID userId);
    List<User> selectAllUser();
    List<UUID> friendListById(UUID userId);
    int updateUserById(UUID userId,User userUpdate);
    int deleteUserById(UUID userId);
    int addFriend(Friend friend);
    int removeFriend(Friend friend);
}
