package com.example.demo.dao;

import com.example.demo.model.Friend;
import com.example.demo.model.User;
import com.example.demo.utilities.FriendList;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.util.*;

@Repository("userDao")
public class UserDao implements IUserDao {
    private final Map<UUID, User> database;
    private final FriendList<UUID> friendList ;

    public UserDao() throws IOException {
        friendList = new FriendList<UUID>();
        database = new HashMap<>();
        UUID userId = UUID.fromString("eb1e105b-4125-4439-909a-ca3928f919fc");
        database.put(userId, new User(userId, "Aja", 24));
    }

    @Override
    public int insertUser(UUID userId, User user) {
        database.put(userId, user);
        return 1;
    }

    @Override
    public User selectUserById(UUID userId) {
        return database.get(userId);
    }

    @Override
    public List<User> selectAllUser() {
        int ss = 00;
        return new ArrayList(database.values());
    }

    @Override
    public int updateUserById(UUID userId, User userUpdate) {
        database.put(userId, userUpdate);
        return 1;
    }

    @Override
    public int deleteUserById(UUID userId) {
        database.remove(userId);
        return 1;
    }

    @Override
    public int addFriend(Friend friend) {
        friendList.addEdge(friend.userId1, friend.userId2);
        return 1;
    }

    @Override
    public List<UUID> friendListById(UUID userId) {
        List<UUID> friends = friendList.getFriendListById(userId);
        return friends;
    }
    @Override
    public int removeFriend(Friend friend) {
        friendList.removeFriend(friend.userId1, friend.userId2);
        return 1;
    }


}

